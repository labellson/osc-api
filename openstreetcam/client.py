"""
Core client functionality. This is common across all API requests
"""

from datetime import datetime
from datetime import timedelta
import time
import random
import requests
import collections

from .exceptions import TransportError


__DEFAULT_BASE_OSC_URL__ = 'https://openstreetcam.org'
__DEFAULT_BASE_OSC_API_URL__ = 'https://api.openstreetcam.org'

_RETRIABLE_STATUSES = set([500, 503, 504])


class Client(object):
    """Performs requests to the OpenStreetCam web services"""

    def __init__(self, access_token=None, timeout=None, retry_timeout=60,
                 queries_per_second=50):
        """
        :param access_token: OpenStreetCam API key.
        :type access_token: string

        :param timeout: Connection timeout. Specify `None` for no timeout.
        :type timeout: int

        :param retry_timeout: Timeout in seconds across retriable requests
        :type retry_timeout: int

        :param queries_per_second: Maximum queries per second. If the number is
            reached, the client will sleep a certain amount of time.
        :type queries_per_second: int
        """
        self.access_token = access_token
        self.timeout = timeout
        self.retry_timeout = timedelta(retry_timeout)

        self.queries_per_second = queries_per_second
        self.queries_sent_times = collections.deque('', queries_per_second)

        self.session = requests.Session()

    def _request(self, url, params=None, first_request_time=None,
                 retry_counter=0, base_url=__DEFAULT_BASE_OSC_API_URL__,
                 post_data=None, post_json=None, request_kwargs=None,
                 extract_body=None):
        """
        Performs HTTP GET/POST with credentials if provided returning the body
        as JSON document

        :param url: URL path for the request. Should begin with a slash.
        :type url: string

        :param params: HTTP GET parameters
        :type params: dict

        :param first_request_time: The time of the first request. `None` if no
            request have occurred.
        :type first_request_time: datetime.datetime

        :param retry_counter: Number of this retry, `0` for the first attemp.
        :type retry_counter: int

        :param base_url: Base URL for the request. Defaults to the
            OpenStreetCam url.
        :type base_url: string

        :param post_data: HTTP POST parameters to send in the request.
        :type post_data: dict

        :param post_json: HTTP POST json to send in the body.
        :type post_json: dict

        :param request_kwargs: Extra arguments for request method
        :type request_kwargs: dict

        :param extract_body: Custom function to extract the body of the
            response in case the response is not JSON
        :type extract_body: function

        :raise: TransportError
        """

        if not first_request_time:
            first_request_time = datetime.now()

        else:
            elapsed_time = datetime.now() - first_request_time
            if elapsed_time > self.retry_timeout:
                raise requests.exceptions.Timeout((
                    'Reached timeout for the request.'
                    f'Elapsed time {elapsed_time}'
                ))

        if retry_counter > 0:
            # 0.5 * (1.5 ^ i). Sleeping time for retry requests
            delay = 0.5 * 1.5 ** (retry_counter - 1)

            # Jitter this value and pause the retry
            time.sleep(delay * (random.random() + .5))

        request_kwargs = request_kwargs if request_kwargs is not None else {}
        # TODO: Build URL parameters

        # Check request is GET or POST
        requests_method = self.session.get
        if post_data is not None or post_json is not None:
            requests_method = self.session.post
            request_kwargs['data'] = post_data
            request_kwargs['json'] = post_json

        # This method can raise exceptions
        try:
            response = requests_method(base_url + url, **request_kwargs)

        except Exception as e:
            raise TransportError(e)

        # Retry request if failed
        if response.status_code in _RETRIABLE_STATUSES:
            return self._request(url, params, first_request_time,
                                 retry_counter + 1, base_url, post_data,
                                 post_json)

        # Sleep the method if max queries in one second are reached
        if len(self.queries_sent_times) == self.queries_per_second:
            elapsed_time_since_first = time.time() - self.queries_sent_times[0]
            if elapsed_time_since_first < 1:
                time.sleep(1 - elapsed_time_since_first)

        # Add time of this request to the queue and return the response
        self.queries_sent_times.append(time.time())

        if extract_body is not None:
            return extract_body(response)
        else:
            return response.json()

    def _generate_url(self, path, params):
        """
        Returns the path adding necessary parameters to the request.

        :param path: path portion of the URL
        :type path: string

        :param params: URL parameters
        :type params: dict

        :rtype: string
        """
        raise NotImplementedError()
