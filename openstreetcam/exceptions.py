class TransportError(Exception):
    "Something went wrong when trying to do the request"

    def __init__(self, base_exception=None):
        self.base_exception = base_exception

    def __str__(self):
        if self.base_exception is not None:
            return str(self.base_exception)

        return 'An unknown error occurred.'
