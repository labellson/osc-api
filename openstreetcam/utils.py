import shutil


def extract_raw_image(response):
    """Extracts an image from a HTTP response and returns the raw stream

    :param response: response http object
    :type response: resquests.model.Response

    :rtype: urllib3.response.HTTPResponse
    """
    response.raw.decode_content = True
    return response.raw


def save_raw_image_on_disk(raw_img, output):
    """Save a raw image that cames from a request

    :param raw_img: raw urllib3 response
    :type raw_img: urllib3.response.HTTPResponse

    :param output: output file name
    :type output: string
    """
    with open(output, 'wb') as f:
        shutil.copyfileobj(raw_img, f)
