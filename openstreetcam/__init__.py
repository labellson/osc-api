__version__ = '0.1.0'

from openstreetcam.client import Client

__all__ = ['Client']
