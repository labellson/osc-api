from os import path as osp
from tqdm import tqdm
import os

from .client import __DEFAULT_BASE_OSC_URL__
from .utils import extract_raw_image, save_raw_image_on_disk
from .exceptions import TransportError


def track_details(client, id):
    """Returns a JSON object with track details

    :param id: id of the track
    :type id: int

    :rtype: dict
    """
    result = client._request('/details', post_data={'id': id})
    status = result['status']['httpCode']
    if status == 200:
        return result['osv']

    else:
        message = result['status']['apiMessage']
        raise Exception((f'HTTP Error: {status} {message}'))


def track_photos(client, id):
    """Returns a list with photos of the track

    :param id: id of the track
    :type id: int

    :rtype: list
    """
    return track_details(client, id).get('photos', [])


def download_track_photo(client, url, output, file_name=None, retry_counter=0):
    """Download a photo from OpenStreetCam storage

    :param url: path part of the url where the photo is
    :type url: string

    :param output: output folder to save the photo
    :type output: string

    :param file_name: filename of the photo
    :type file_name: string

    :param retry_counter: Number of this retry in case of exceptions. `0` for
        the first attemp.
    :type retry_counter: int
    """
    if not osp.exists(output):
        os.makedirs(output)

    if url[0] != '/':
        url = '/' + url

    if file_name is None:
        file_name = osp.basename(url)

    try:
        raw_img = client._request(url, base_url=__DEFAULT_BASE_OSC_URL__,
                                  extract_body=extract_raw_image,
                                  request_kwargs=dict(stream=True),
                                  retry_counter=retry_counter)
    except TransportError as te:
        print('[Exception] ', te)
        print('Retrying...\n')
        return download_track_photo(client, url, output, file_name,
                                    retry_counter + 1)

    save_raw_image_on_disk(raw_img, osp.join(output, file_name))
